package com.example.travelling;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.travelling.activity.HomeActivity;
import com.example.travelling.adapter.ChatListAdaptor;

public class ChatActivity extends AppCompatActivity {


    RecyclerView rvChats;
    ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        findViewByIds();

        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChatActivity.this, HomeActivity.class));
                finish();
            }
        });
    }

    private void findViewByIds() {
        rvChats = findViewById(R.id.rvChats);
        ChatListAdaptor adaptor = new ChatListAdaptor(this);
        rvChats.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
        rvChats.setAdapter(adaptor);

        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

}
