package com.example.travelling.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.travelling.Interface.SelectedListner;
import com.example.travelling.R;

public class FeedAdaptor extends RecyclerView.Adapter<FeedAdaptor.MyViewHolder> {

    Context context;
    SelectedListner listner;
    public FeedAdaptor(Context context, SelectedListner listner) {
        this.context = context;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

//        holder.iv_fb_with_video.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listner.onProfileClick(position);
//            }
//        });
//        holder.ivchat.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listner.onChatClick(position);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return 7;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_fb_with_video,ivchat;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
//            iv_fb_with_video =itemView.findViewById(R.id.iv_fb_with_video);
//            ivchat =itemView.findViewById(R.id.ivchat);
        }
    }
}
