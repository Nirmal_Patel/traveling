package com.example.travelling.util;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import com.example.travelling.R;

public class Common {

    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.color.nGreenA);
            window.addFlags(Integer.MIN_VALUE);
            window.setStatusBarColor(activity.getResources().getColor(R.color.nGreenA));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.white));
            window.setBackgroundDrawable(background);
        }
    }

    public static void hideKeyboard(Activity context) {
        InputMethodManager imm = (InputMethodManager)
                context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(context.getWindow().getDecorView().getWindowToken(), 0);
    }


    public static void setStatusBarSignup(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.color.nOrangeB);
            window.addFlags(Integer.MIN_VALUE);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }
}
