package com.example.travelling.Interface;

public interface SelectedListner {

    void onProfileClick(int pos);
    void onDeleteClick(int pos);
    void onChatClick(int pos);
    void onAsignClick(int pos);
    void onViewClick(int pos);
}
