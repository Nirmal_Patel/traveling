package com.example.travelling.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.travelling.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static com.example.travelling.util.Common.setStatusBarSignup;

public class SignupActivity extends AppCompatActivity {

    EditText et_name, et_Dob, et_phone;
    Button btnSignup;
    TextView tvLogin;
    private DatePickerDialog brithDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        setStatusBarSignup(SignupActivity.this);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(android.R.color.white));
        }

        init();
        font();
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(SignupActivity.this, OtpActivity.class));
                finish();
            }
        });

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(SignupActivity.this, LoginActivity.class));
            }
        });
        et_Dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateTimeField();
                brithDatePickerDialog.show();
            }
        });
    }

    private void setDateTimeField() {
        Calendar newCalender = Calendar.getInstance();
        brithDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthofyera, int dayofmonth) {
                Calendar userAge = new GregorianCalendar(year, monthofyera, dayofmonth);
                Calendar minAdultAge = new GregorianCalendar();
                minAdultAge.add(Calendar.YEAR, -12);
                if (minAdultAge.before(userAge)) {
                    Toast.makeText(SignupActivity.this, "Please Select Valid Date", Toast.LENGTH_SHORT).show();
                    return;
                }
                Date current = userAge.getTime();
                int diff1 = new Date().compareTo(current);

                if (diff1 < 0) {
                    Toast.makeText(SignupActivity.this, "Please Select Valid Date", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    et_Dob.setText(dateFormatter.format(userAge.getTime()));
                }
            }
        }, newCalender.get(Calendar.YEAR),
                newCalender.get(Calendar.MONTH),
                newCalender.get(Calendar.DAY_OF_MONTH));
    }

    private void init() {
        et_name = findViewById(R.id.et_name);
        et_Dob = findViewById(R.id.et_Dob);
        et_phone = findViewById(R.id.et_phone);
        tvLogin = findViewById(R.id.tvLogin);
        btnSignup = findViewById(R.id.btnSignup);
    }

    private void font() {
        Typeface myCustomFont1 = Typeface.createFromAsset(getAssets(), "fonts/OpenSans_R.ttf");
//        Typeface myCustomFont1 = Typeface.createFromAsset(getAssets(), "fonts/breeserif.ttf");
        et_name.setTypeface(myCustomFont1);
        et_Dob.setTypeface(myCustomFont1);
        et_phone.setTypeface(myCustomFont1);
        btnSignup.setTypeface(myCustomFont1);


    }

}
