package com.example.travelling.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.travelling.R;

import static com.example.travelling.util.Common.setStatusBarGradiant;

public class LoginActivity extends AppCompatActivity {


    TextView tvSignup;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setStatusBarGradiant(LoginActivity.this);

        init();
        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                finish();
            }
        });

    }

    private void init() {
        tvSignup = findViewById(R.id.tvSignup);
        btnLogin = findViewById(R.id.btnLogin);
    }
}
