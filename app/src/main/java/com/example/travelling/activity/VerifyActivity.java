package com.example.travelling.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.travelling.R;

public class VerifyActivity extends AppCompatActivity {

    Button btnVerify;
    ImageView ivDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_verify);

        btnVerify = findViewById(R.id.btnVerify);
        ivDone = findViewById(R.id.ivDone);

        Glide.with(this).load(R.drawable.veify_done).into(ivDone);
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(VerifyActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}
