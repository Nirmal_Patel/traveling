package com.example.travelling.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.travelling.R;

public class SplashActivity extends AppCompatActivity {

    ImageView ivSplash;
    TextView tvAppName;
    private static final long SPLASH_TIME_OUT = 7500;
    private boolean mIsBackButtonProcessed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        tvAppName = findViewById(R.id.tvAppName);
        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.buttom_up);
        anim.setDuration(2200);
        tvAppName.startAnimation(anim);

        ivSplash = findViewById(R.id.ivSplash);
        Glide.with(this).load(R.drawable.bg_c).into(ivSplash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                if (!mIsBackButtonProcessed) {
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }
}
