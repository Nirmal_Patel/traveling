package com.example.travelling.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.example.travelling.R;
import com.example.travelling.util.Common;

public class OtpActivity extends AppCompatActivity {

    EditText etOtp1, etOtp2, etOtp3, etOtp4;
    Button btnVerify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        setStatusBarGradiant(OtpActivity.this);
        findViewIds();
        init();

    }

    private void init() {
        etOtp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    etOtp1.clearFocus();
                    if (etOtp2.getText().toString().length() == 0) {
                        etOtp2.requestFocus();
                    } else if (etOtp3.getText().toString().length() == 0) {
                        etOtp3.requestFocus();
                    } else if (etOtp4.getText().toString().length() == 0) {
                        etOtp4.requestFocus();
                    } else {
                        Common.hideKeyboard(OtpActivity.this);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etOtp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etOtp2.clearFocus();
                if (charSequence.length() > 0) {

                    if (etOtp3.getText().toString().length() == 0) {
                        etOtp3.requestFocus();
                    } else if (etOtp4.getText().toString().length() == 0) {
                        etOtp4.requestFocus();
                    } else {
                        if (etOtp1.getText().toString().length() == 0) {
                            etOtp1.requestFocus();
                        } else {
                            Common.hideKeyboard(OtpActivity.this);
                        }

                    }
                } else {
                    etOtp1.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etOtp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etOtp3.clearFocus();
                if (charSequence.length() > 0) {

                    if (etOtp4.getText().toString().length() == 0) {
                        etOtp4.requestFocus();
                    } else {

                        if (etOtp1.getText().toString().length() == 0) {
                            etOtp1.requestFocus();
                        } else if (etOtp2.getText().toString().length() == 0) {
                            etOtp1.requestFocus();
                        } else {
                            Common.hideKeyboard(OtpActivity.this);
                        }

                    }

                } else {
                    etOtp2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etOtp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etOtp4.clearFocus();
                if (charSequence.length() > 0) {

                    btnVerify.requestFocus();


                    if (etOtp1.getText().toString().length() == 0) {
                        etOtp1.requestFocus();
                    } else if (etOtp2.getText().toString().length() == 0) {
                        etOtp1.requestFocus();
                    } else if (etOtp3.getText().toString().length() == 0) {
                        etOtp3.requestFocus();
                    } else {
                        Common.hideKeyboard(OtpActivity.this);
                    }


                } else {
                    etOtp3.requestFocus();

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Common.hideKeyboard(OtpActivity.this);
            }
        });

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(OtpActivity.this, VerifyActivity.class));
                finish();

            }
        });

    }


    private void findViewIds() {

        etOtp1 = findViewById(R.id.etOtp1);
        etOtp2 = findViewById(R.id.etOtp2);
        etOtp3 = findViewById(R.id.etOtp3);
        etOtp4 = findViewById(R.id.etOtp4);
        btnVerify = findViewById(R.id.btnVerify);

    }


    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.color.white);
            window.addFlags(Integer.MIN_VALUE);
            window.setStatusBarColor(activity.getResources().getColor(R.color.nBlueA));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.white));
            window.setBackgroundDrawable(background);
        }
    }
}
