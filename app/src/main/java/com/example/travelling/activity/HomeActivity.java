package com.example.travelling.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.example.travelling.R;
import com.example.travelling.fragment.FeedsFragment;
import com.example.travelling.fragment.ProfileFragment;
import com.example.travelling.fragment.SupportFragment;
import com.example.travelling.fragment.TransactionFragment;
import com.example.travelling.fragment.WalletFragment;
import com.example.travelling.util.BottomNavigationViewHelper;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home);
        findviewIds();
        init();

        setFragment(new FeedsFragment());
    }

    private void init() {
        BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
                = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment;
                switch (item.getItemId()) {
                    case R.id.navigation_feed:
                        setFragment(new FeedsFragment());

                        return true;
                    case R.id.nav_wallet:
                        setFragment(new WalletFragment());

                        return true;
                    case R.id.nav_service:
                        setFragment(new SupportFragment());

                        return true;
                    case R.id.nav_profile:
                        setFragment(new ProfileFragment());

                        return true;
                }
                return false;
            }
        };
        bottomNavigationView.setItemIconTintList(null);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void findviewIds() {
        bottomNavigationView = findViewById(R.id.bottom_navigation);
    }

    void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();
    }


}
