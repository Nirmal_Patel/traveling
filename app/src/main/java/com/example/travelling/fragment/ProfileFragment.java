package com.example.travelling.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.travelling.R;
import com.example.travelling.activity.LoginActivity;


public class ProfileFragment extends Fragment {

    private TextView tvProfileSetting, tvChangePws;
    LinearLayout llLogout;
    LinearLayout llProfile, llMyProfile;
    String sFrom = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            sFrom = args.getString("isFrom");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        init(view);

        if (sFrom.equals("")) {
            llMyProfile.setVisibility(View.VISIBLE);
            llProfile.setVisibility(View.GONE);
        } else {
            llMyProfile.setVisibility(View.GONE);
            llProfile.setVisibility(View.VISIBLE);
        }

        clik();
        setStatusBarGradiant(getActivity());
        return view;
    }

    public void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.color.white);
            window.addFlags(Integer.MIN_VALUE);
            window.setStatusBarColor(activity.getResources().getColor(R.color.white));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.white));
            window.setBackgroundDrawable(background);
        }
    }

    private void clik() {
        tvProfileSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new EditProfileFragment()).commit();
            }
        });

        tvChangePws.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new ChangePwsFragment()).commit();
            }
        });
        llLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            }
        });
    }

    private void init(View view) {
        tvProfileSetting = view.findViewById(R.id.tvProfileSetting);
        tvChangePws = view.findViewById(R.id.tvChangePws);
        llLogout = view.findViewById(R.id.llLogout);
        llProfile = view.findViewById(R.id.llProfile);
        llMyProfile = view.findViewById(R.id.llMyProfile);
    }


}
