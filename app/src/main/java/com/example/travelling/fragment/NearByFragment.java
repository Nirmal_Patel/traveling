package com.example.travelling.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.travelling.R;

public class NearByFragment extends Fragment {

    ImageView ivSearch;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_near_by, container, false);
        init(view);
        return view;
    }

    private void init(View view) {

        ivSearch =view.findViewById(R.id.ivSearch);
        Glide.with(this).load(R.drawable.ic_nearby).into(ivSearch );
    }

}
