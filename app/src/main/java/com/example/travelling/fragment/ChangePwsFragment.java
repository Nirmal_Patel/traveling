package com.example.travelling.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.example.travelling.R;

public class ChangePwsFragment extends Fragment {


    LinearLayout llSubmit;
    ImageView ivImage, ivBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_pws, container, false);
        init(view);
        click();
        return view;
    }

    private void click() {
        llSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new ProfileFragment()).commit();
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new ProfileFragment()).commit();
            }
        });

    }

    private void init(View view) {
        llSubmit = view.findViewById(R.id.llSubmit);
        ivBack = view.findViewById(R.id.ivBack);
        ivImage = view.findViewById(R.id.ivImage);
        Glide.with(this).load(R.drawable.bg_c).into(ivImage);
    }

}
