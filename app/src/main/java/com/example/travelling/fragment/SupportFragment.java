package com.example.travelling.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.travelling.R;

public class SupportFragment extends Fragment {


    ImageView ivNote;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_support, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        ivNote =view.findViewById(R.id.ivNote);
        Glide.with(this).load(R.drawable.bg_support).into(ivNote );
    }

}
