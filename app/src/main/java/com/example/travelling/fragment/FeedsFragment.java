package com.example.travelling.fragment;


import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.travelling.ChatActivity;
import com.example.travelling.Interface.SelectedListner;
import com.example.travelling.R;
import com.example.travelling.activity.SignupActivity;
import com.example.travelling.adapter.FeedAdaptor;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class FeedsFragment extends Fragment {

    FeedAdaptor adaptor;
    TextInputEditText et_date;
    ArrayList<String> ArryList = new ArrayList<>();
    ImageView ivSearch,ivAdd;
    private DatePickerDialog brithDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    ImageView tv_filter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feeds, container, false);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        ivSearch = view.findViewById(R.id.ivSearch);
        tv_filter = view.findViewById(R.id.tv_filter);

        tv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailogChangeDestination(getActivity());
            }
        });

        ivAdd = view.findViewById(R.id.ivAdd);
        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new NearByFragment()).commit();
            }
        });

        RecyclerView rv_feed = view.findViewById(R.id.rv_feeds);
        adaptor = new FeedAdaptor(getActivity(), new SelectedListner() {
            @Override
            public void onProfileClick(int pos) {

                ProfileFragment fragmentB = new ProfileFragment();
                Bundle args = new Bundle();
                args.putString("isFrom", "home");
                fragmentB.setArguments(args);
                getFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, fragmentB)
                        .commit();
            }

            @Override
            public void onDeleteClick(int pos) {

            }

            @Override
            public void onChatClick(int pos) {
                Intent i = new Intent(getActivity(), ChatActivity.class);
                startActivity(i);
            }

            @Override
            public void onAsignClick(int pos) {

            }

            @Override
            public void onViewClick(int pos) {

            }
        });
        rv_feed.setAdapter(adaptor);

        return view;
    }


    private void dailogChangeDestination(Context mContext) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_search);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        TextInputEditText et_city = dialog.findViewById(R.id.et_city);
        et_date = dialog.findViewById(R.id.et_date);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);


        et_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateTimeField();
                brithDatePickerDialog.show();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void setDateTimeField() {
        Calendar newCalender = Calendar.getInstance();
        brithDatePickerDialog = new DatePickerDialog(getActivity(), android.R.style.Theme_DeviceDefault_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthofyera, int dayofmonth) {
                Calendar userAge = new GregorianCalendar(year, monthofyera, dayofmonth);
                et_date.setText(dateFormatter.format(userAge.getTime()));
            }
        }, newCalender.get(Calendar.YEAR),
                newCalender.get(Calendar.MONTH),
                newCalender.get(Calendar.DAY_OF_MONTH));
    }
}
